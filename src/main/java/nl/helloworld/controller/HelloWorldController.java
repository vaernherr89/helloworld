package nl.helloworld.controller;

import nl.helloworld.dto.Greeting;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Produces;

@RestController
@RequestMapping("/greeting")
public class HelloWorldController {

    @GetMapping(value = "/hello")
    @Produces({"application/json"})
    public ResponseEntity<Greeting> hello() {
        Greeting greeting = new Greeting();
        greeting.setPhrase("Hello World!");
        return new ResponseEntity<>(greeting, HttpStatus.OK);
    }
}
